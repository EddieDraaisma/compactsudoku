//
//  main.swift
//  SudokuCompact
//
//  Created by Eddie Draaisma on 05/02/2019.
//  Copyright © 2019 Eddie Draaisma. All rights reserved.
//

import Foundation


struct colConstraint: Hashable {
    
    let text: String
    let value1: Int
    let value2: Int
    
    init(_ t: String, _ v1: Int, _ v2: Int) { text = t; value1 = v1; value2 = v2 }
}


struct rowSolution: Hashable, Equatable {
    
    let row: Int
    let col: Int
    let value: Int
    init(_ r: Int, _ c: Int, _ v: Int) { row = r; col = c; value = v }
}


func rowConstraints(row r: Int, col c: Int, val v: Int) -> [colConstraint] {
    
    let b = (r / 3) * 3 + c / 3
    let celConstraints = [colConstraint("rc", r, c),
                          colConstraint("rn", r, v),
                          colConstraint("cn", c, v),
                          colConstraint("bn", b, v)]
    return celConstraints
}



func select<T, Z>(_ X: inout [Z: Set<T>], _ Y: [T: [Z]], _ row: T) -> [Set<T>] {
    
    var cols: [Set<T>] = []
    for constraint1 in Y[row]! {
        for cell in X[constraint1]! {
            for constraint2 in Y[cell]! {
                if constraint1 != constraint2 {
                    X[constraint2]!.remove(cell)
                }
            }
        }
        cols.append(X.removeValue(forKey: constraint1)!)
    }
    return cols
}



func deselect<T, Z>(_ X: inout [Z: Set<T>], _ Y: [T: [Z]], _ row: T, _ cols: inout [Set<T>]) {
    for constraint1 in Y[row]!.reversed() {
        X[constraint1] = cols.popLast()
        for cell in X[constraint1]! {
            for constraint2 in Y[cell]! {
                if constraint2 != constraint1 {
                    X[constraint2]!.insert(cell)
                }
            }
        }
    }
}



var counter = 0

func solve<T, Z>(_ X: [Z: Set<T>], _ Y: [T: [Z]]) -> [T] {
    
    var solution: [T] = []
    var X = X
    
    func solveLocal(_ X: inout [Z: Set<T>], _ Y: [T: [Z]]) -> [T]? {
        
        counter += 1
        
        if X.isEmpty {
            return solution
        }
        else {
            let c = X.min(by: {$0.value.count < $1.value.count})!
            for row in c.value {
                solution.append(row)
                var cols = select(&X, Y, row)
                if let s = solveLocal(&X, Y) {
                    return s
                }
                deselect(&X, Y, row, &cols)
                _ = solution.popLast()
            }
        }
        return nil
    }
    return solveLocal(&X, Y) ?? []
}


func makeBoard(_ gameStr: String) -> [Int] {
    
    var board = Array(repeating: 0, count: 81)
    var pos = 0
    for chr in gameStr {
        board[pos] = Int(String(chr)) ?? 0
        pos += 1
    }
    return board
}



func solveBoard(_ board: [Int]) -> [Int] {
    var board = board
    
    
    var Y: [rowSolution: [colConstraint]] = [:]
    var X: [colConstraint: Set<rowSolution>] = [:]
    
    for row in 0 ..< 9 {
        for col in 0 ..< 9 {
            for num in 1 ... 9 {
                let box = (row / 3) * 3 + col / 3
                let celConstraints = [colConstraint("rc", row, col),
                                      colConstraint("rn", row, num),
                                      colConstraint("cn", col, num),
                                      colConstraint("bn", box, num)]
                Y[rowSolution(row, col, num)] = celConstraints
                for constraint in celConstraints {
                    X[constraint, default: []].insert(rowSolution(row, col, num))
                }
            }
        }
    }
    for pos in 0 ..< 81 {
        if board[pos] != 0 {
            let _ = select(&X, Y, rowSolution(pos / 9, pos % 9, board[pos]))
        }
    }
    let solution = solve(X, Y)
    for row in solution {
        board[row.row * 9 + row.col] = row.value
    }
    return board
}


func printBoard(_ board: [Int]) {
    print()
    for row in 0 ..< 9 {
        if [3, 6].contains(row) {
            print(" ------+-------+------")
        }
        print(" ", terminator: "")
        for col in 0 ..< 9 {
            if [3, 6].contains(col) {
                print("| ", terminator: "")
            }
            print(board[row * 9 + col], terminator: " ")
        }
        print()
    }
    print()
}


let grid1 = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
let hard1 = ".....6....59.....82....8....45........3........6..3.54...325..6.................."
let most  = "061007003092003000000000000008530000000000504500008000040000001000160800600000000"
let grid2 = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
let difficult = "800000000003600000070090200050007000000045700000100030001000068008500010090000400"

let diff = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
let diff2 = "980700000600090800005008090500006080000400300000070002090300700050000010001020004"

let xx = "...8.1..........435............7.8........1...2..3....6......75..34........2..6.."

let snyder = " 5  2  3 2    17 84 76          5   52     47   7          35 43 65    1 9  7  6 "


let gameStr = most



printTimeElapsedWhenRunningCode {
    printBoard(solveBoard(makeBoard(gameStr)))
}

print(counter)
